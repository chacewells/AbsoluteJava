package ch14.generics;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;

public class InputFile {

	public static void main(String[] args) {
		File scoreInfo = new File("scoreinfo.txt");
		PrintWriter scoreGen = null;
		
		try {
			scoreGen = new PrintWriter(scoreInfo);
			
			scoreGen.printf("%.1f%n", Math.random()*3.8);
			for (int i = 0; i < 7; i++) {
				scoreGen.printf("%.1f ", Math.random()*10.0);
			}
			scoreGen.close();
			System.out.println("Successful file write!");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
