package ch14.generics;

public class Somebody implements Comparable<Somebody> {
	private String first;

	private String last;
	
	public static void main(String[] args) {
		Somebody me = new Somebody("Aaron", "Wells");
		Somebody jenni = new Somebody("Jenni", "Wells");
		System.out.println(jenni.compareTo(me));
	}
	
	public Somebody(String first, String last) {
		this.first = first;
		this.last = last;
	}
	public String getFirst() {
		return first;
	}

	public void setFirst(String first) {
		this.first = first;
	}

	public String getLast() {
		return last;
	}

	public void setLast(String last) {
		this.last = last;
	}

	@Override
	public int compareTo(Somebody o) {
		if (this.last.compareToIgnoreCase(o.getLast()) < 0)
			return -1;
		if (this.last.compareToIgnoreCase(o.getLast()) > 0)
			return 1;
		if (this.last.equalsIgnoreCase(o.getLast())) {
			if (this.first.compareToIgnoreCase(o.getFirst()) < 0)
				return -1;
			if (this.first.compareToIgnoreCase(o.getFirst()) > 0)
				return 1;
		}
		
		return 0;
	}

}
