package ch14.generics;

import java.util.ArrayList;
import java.util.Collections;
import java.util.InputMismatchException;
import java.util.ListIterator;
import java.util.Scanner;

public class ContactBook {
	private Scanner keyboard = new Scanner(System.in);
	private ArrayList<Contact> contacts;
	
	public static void main(String[] args) {
		ContactBook book = new ContactBook();
		
		while (true) {
			book.showMenu();
			book.doAction(book.readAction());
		}
	}
	
	public ContactBook() {
		contacts = new ArrayList<>();
	};

	public ContactBook(ArrayList<Contact> contacts) {
		this.contacts = contacts;
	}
	
	public ArrayList<Contact> getContacts() {
		ArrayList<Contact> contactsCopy = new ArrayList<Contact>();
		Collections.copy(contactsCopy, contacts);
		return contactsCopy;
	}

	public void setContacts(ArrayList<Contact> contacts) {
		this.contacts = contacts;
	}
	
	public void addContact() {
		Contact newContact = new Contact();
		
		readFirstName(newContact);
		readLastName(newContact);
		readPhone(newContact);
		readEmail(newContact);
		
		contacts.add(newContact);
	}
	
	public String toString() {
		String allContacts = "";
		for (Contact c : contacts)
			allContacts += c+"\n";
		return allContacts;
	}
	
	public void searchContacts(String search) {
		try {
			ArrayList<Contact> results = new ArrayList<>(1);
			String[] searchArray = search.split(" ");
			for (Contact c : contacts) {
				if (c.getFirstName().contains(searchArray[0]))
					results.add(c);
				else if (c.getLastName().contains(searchArray[searchArray.length-1]))
					results.add(c);
				else if (c.getPhone().contains(search))
					results.add(c);
				else if (c.getEmail().contains(search))
					results.add(c);
			}
			
			if (results.size() < 1)
				throw new ContactNotFoundException();
			
			System.out.println("Search results:");
			for (Contact c : results) {
				System.out.println(c);
			}
			
			confirmDeleteRecords(results);
		} catch (ContactNotFoundException e) {
			System.out.println(e.getMessage());
			return;
		}
		
	}
	
	private void confirmDeleteRecords(ArrayList<Contact> searchResults) {
		System.out.println("Would you like to delete this/these records?");
		String answer;
		do {
			System.out.println("Answer 'yes' or 'no'");
			answer = keyboard.nextLine();
		} while (!answer.equals("yes") && !answer.equals("no"));
		
		switch(answer) {
		case "yes":
			deleteRecords(searchResults);
			break;
		case "no":
			return;
		default:
			return;
		}
	}
	
	private void deleteRecords(ArrayList<Contact> searchResults) {
		ListIterator<Contact> it = searchResults.listIterator();
		while (it.hasNext()) {
			contacts.remove(searchResults.get(it.nextIndex()));
			it.next();
		}
		System.out.println("The following records successfully deleted:");
		for (Contact c : searchResults)
			System.out.println(c);
	}
	
	public void showMenu() {
		System.out.println("What would you like to do?");
		System.out.println("[1]\tSearch contacts");
		System.out.println("[2]\tAdd contact");
		System.out.println("[3]\tQuit");
	}
	
	public int readAction() {
		int input = 0;
		boolean done = false;
		do {
			try {
				input = keyboard.nextInt();
				if (input < 1 || input > 3)
					throw new InputMismatchException();
				done = true;
			} catch (InputMismatchException e) {
				keyboard.nextLine();
				System.out.println("Please enter a valid option (1, 2, 3)");
				input = 0;
			}
		} while (!done);
		
		return input;
	}
	
	public void doAction(int action) {
		switch (action) {
		case 1:
			System.out.println("Who would you like to search for?");
			keyboard.nextLine();
			searchContacts(keyboard.nextLine());
			break;
		case 2:
			addContact();
			break;
		case 3:
			System.out.println("Goodbye!");
			System.exit(0);
		default:
			System.out.println("User input error. Exiting...");
			System.exit(1);
		}
	}
	
	public void readFirstName(Contact contact) {
		do {
			keyboard.nextLine();
			System.out.println("Enter the first name:");
			contact.setFirstName(keyboard.nextLine());
		} while (!contact.isValidName(contact.getFirstName()));
	}
	
	public void readLastName(Contact contact) {
		do {
			System.out.println("Enter the last name:");
			contact.setLastName(keyboard.nextLine());
		} while (!contact.isValidName(contact.getLastName()));
	}
	
	public void readPhone(Contact contact) {
		do {
			System.out.println("Enter the phone number:");
			contact.setPhone(keyboard.nextLine());
		} while (!contact.isValidPhone(contact.getPhone()));
	}
	
	public void readEmail(Contact contact) {
		do {
			System.out.println("Enter the email:");
			contact.setEmail(keyboard.nextLine());
		} while (!contact.isValidEmail(contact.getEmail()));
	}
	
	private class ContactNotFoundException extends Exception {
		public ContactNotFoundException() {
			super("Contact not found!");
		}
	}
	
}
