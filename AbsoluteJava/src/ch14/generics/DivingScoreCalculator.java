/*
 * DivingScoreCalculator.java
 * An simple, interactive program that calculates a diver's overall
 *+score from the judges' individual scores.
 * Demonstrates usage of Java ArrayLists 
 * 
 * Author: Aaron Wells
 */

package ch14.generics;

import java.util.ArrayList;
import java.util.Collections;
import java.util.InputMismatchException;
import java.util.Scanner;

public class DivingScoreCalculator {
	
	// A constant used to calculate the diver's overall score
	public static final double SCORE_MULTIPLIER = 0.6;
	
	// Some more fields.  Self-explanatory
	// judgeScores is a list of Double objects
	private double difficulty;
	private ArrayList<Double> judgeScores;
	private Scanner keyboard = new Scanner(System.in);
	
	// Initializes 7 judge scores to invalid scores
	public DivingScoreCalculator() {
		judgeScores = new ArrayList<>(7);
		for (int i = 0; i < 7; i++)
			judgeScores.add(-1.0);
	} // DivingScoreCalculator

	// Main
	public static void main(String[] args) {
		DivingScoreCalculator dCalc = new DivingScoreCalculator();
		
		dCalc.readDifficulty();
		dCalc.readJudgeScores();
		
		System.out.printf("Diver's final score: %.1f", dCalc.getDiverScore());
	} // Main

	// Gets dive difficulty level from user and ensures between 1.2 and 3.0
	public void readDifficulty() {
		do {
			try {
				System.out.println("How difficult was the dive?");
				difficulty = keyboard.nextDouble();
			} catch (InputMismatchException i) {
				keyboard.nextLine();
				difficulty = 0;
			}
		} while (difficulty < 1.2 || difficulty > 3.8);
	} // readDifficulty

	// Gets judge scores from user and ensures validity
	public void readJudgeScores() {
		for (int i = 0; i < judgeScores.size(); i++) {
			do {
				System.out.printf("Please enter a score for" +
						" judge no. %d%n", i+1);
				judgeScores.set(i, keyboard.nextDouble());
			} while (judgeScores.get(i).compareTo(.0) < 0
					|| judgeScores.get(i).compareTo(10.0) > 0);
		}
	} // readJudgeScores
	
	/*
	 * Calculates diver score by the following procedure:
	 * The highest and lowest scores are thrown out.
	 * The scores are added.
	 * The sum is multiplied by the difficulty and then by 0.6.
	 * The result is the diver's overall score.
	 */
	public double getDiverScore() {
		Collections.sort(judgeScores);				// sort to identify highest
		judgeScores.remove(judgeScores.size()-1);	// and lowest scores easily
		judgeScores.remove(0);
		
		double sum = 0;					// Sum of remaining scores calculated here.
		for (Double d : judgeScores)
			sum += d;
		
		double total = sum * difficulty;
		return total * SCORE_MULTIPLIER; // This is the overall score.
	}

}
