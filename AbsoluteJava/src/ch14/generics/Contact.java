package ch14.generics;

import java.util.Scanner;


public class Contact {
	protected Scanner keyboard = new Scanner(System.in);
	private String firstName, lastName, phone, email;
	
	public Contact() {}
	
	public Contact(String firstName, String lastName) {
		this.firstName = firstName;
		this.lastName = lastName;
		phone = null;
		email = null;
	}
	
	public Contact(String firstName, String lastName,
			String phone) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.phone = phone;
		this.email = null;
	}
	
	public Contact(String firstName, String lastName,
			String phone, String email) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.phone = phone;
		this.email = email;
	}
	
	public String toString() {
		if (null != phone && null != email)
			return String.format("%s %s\t%s\t%s",
					firstName, lastName, phone, email);
		else if (null == phone && null != email)
			return String.format("%s %s\t%s",
					firstName, lastName, email);
		else if (null == email && null != phone)
			return String.format("%s %s\t%s",
					firstName, lastName, phone);
		else return String.format("%s %s", firstName, lastName);
	}
	
	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	public boolean isValidName(String name) {
		for (Character c : name.toCharArray())
			if (!Character.isLetter(c)) return false;
		return true;
	}
	
	public boolean isValidPhone(String phone) {
		for (Character c : phone.toCharArray())
			if (!Character.isDigit(c) && !c.equals('-'))
				return false;
		return true;
	}
	
	public boolean isValidEmail(String email) {
		String[] emailArray = email.split("@", 2);
		if (emailArray.length < 2) return false;
		return true;
	}
}
