package ch14.generics;

import java.util.ArrayList;

public class MyMathClass<T extends Number> {
	public ArrayList<T> myArray;

	public static void main(String[] args) {
		MyMathClass<Double> intMath = new MyMathClass<>();
		intMath.myArray = new ArrayList<>(10);
		
		System.out.println("Here is a list of doubles:");
		for (int i = 0; i < 10; i++) {
			intMath.myArray.add(Math.random()*10);
			if (i == 9)
				System.out.printf("%.2f", intMath.myArray.get(i));
			else
				System.out.printf("%.2f, ", intMath.myArray.get(i));
		}
		System.out.println();
		
		System.out.printf("Their standard deviation: %.5f", standardDeviation(intMath.myArray));
	}
	
	public static <T extends Number> double standardDeviation (ArrayList<T> numberList) {
		double sum = 0;
		
		for (T t : numberList)
			sum += t.doubleValue();
		
		// calculate mean
		double mean = sum / (double) numberList.size();
		
		// sum now keeps track of each number less the mean
		sum = .0;
		for (T t : numberList)
			sum += Math.pow((t.doubleValue() - mean), 2.0);
		
		double meanOfSqDiffs = sum / (double) numberList.size();
		
		// the standard deviation is the square root of: mean of squared differences 
		return Math.pow(meanOfSqDiffs, 0.5);
	}

}
