package ch14.generics;

import java.util.ArrayList;

public class Job {
	public int id;
	public String title;
	public String company;
	private ArrayList<String> skills;
	
	public ArrayList<String> getSkills() {
		return new ArrayList<String>(this.skills);
	}
	
	public void addSkill(String skill) {
		skills.add(skill);
	}
	
	public void removeSkill(String skill) {
		for (String s : skills) {
			if (s.equals(skill))
				skills.remove(s);
		}
	}
}
