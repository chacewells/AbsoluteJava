package exceptions;

public class PowerFailureException extends Exception {
	public PowerFailureException() {
		super("Power failure!");
	}
	
	public PowerFailureException(String message) {
		super(message);
	}
	
}
