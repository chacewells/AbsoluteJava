package exceptions;

import java.util.InputMismatchException;
import java.util.Scanner;

public class InputMismatchExceptionDemo {
	public static void main(String[] args) {
		Scanner keyboard = new Scanner(System.in);
		int myNum = getInt(keyboard);
		System.out.println(myNum);
		keyboard.close();
	}
	
	private static int getInt(Scanner keyboard) {
		int number = 0; //keeps compiler happy
		boolean done = false;
		
		while(!done) {
			try {
				System.out.println("Enter a whole number:");
				number = keyboard.nextInt();
				done = true;
			} catch(InputMismatchException e) {
				keyboard.nextLine();
				System.out.println("Not a correctly written whole number.");
				System.out.println("Try again.");
			}
		}
			return number;
	}
}
