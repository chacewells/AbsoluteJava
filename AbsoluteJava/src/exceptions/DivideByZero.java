package exceptions;

public class DivideByZero {
	public static void main(String[] anderson) {
		int numerator = 5;
		int denominator = 0;
		System.out.println("numerator: " + numerator);
		System.out.println("denominator: " + denominator);
		try {
			double product = numerator / (double) denominator;
			System.out.println("product: " + product);
		} catch(Exception e) {
			System.out.println("exception message: " + e.getMessage());
		}
	}
}
