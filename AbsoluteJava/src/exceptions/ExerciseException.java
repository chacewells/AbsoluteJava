package exceptions;

public class ExerciseException extends Exception {
	public ExerciseException() {
		super("Exercise exception thrown!");
		System.out.println("Exception thrown!");
	}
	
	public ExerciseException(String message) {
		super(message);
		System.out.println("Exception thrown with message!");
	}
	
	public static void main(String[] poop) {
		ExerciseException e = new ExerciseException();
		System.out.println(e.getMessage());
	}
}
