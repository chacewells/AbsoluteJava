package exceptions;

public class MyException extends Exception {
	public MyException() {
		super("My exception thrown!");
	}
	
	public MyException(String message) {
		super("MyException: " + message);
	}
	
	public static void main(String[] gloomy) {
		int number;
		try {
			System.out.println("Try block entered.");
			number = 42;
			if (number > 0)
				throw new MyException("Hi Mom!");
			System.out.println("Leaving try block");
		} catch (Exception e){
			System.out.println(e.getMessage());
		}
	}
}
