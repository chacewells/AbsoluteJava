package exceptions;

import java.util.Scanner;

public class TooMuchStuffException extends Exception {
	public TooMuchStuffException() {
		super("Too much stuff!");
	}
	
	public TooMuchStuffException(String message) {
		super(message);
	}
	
	public static void main(String[] boots) {
		Scanner keyboard = new Scanner(System.in);
		try {
			System.out.println("How much stuff do you have?");
			String answer = keyboard.nextLine();
			System.out.println(answer);
			keyboard.close();
			throw new TooMuchStuffException("You gotta take a load off, brother!");
		} catch(TooMuchStuffException t) {
			System.out.println(t.getMessage());
		}
	}
}
