package ch18.gui;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Scanner;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JTextArea;

public class FontSampler extends JFrame implements ActionListener {
	
	public static final int WIDTH = 800;
	public static final int HEIGHT = 600;
	
	private JTextArea displayArea;
//	private JPanel fontDisplayPane;
	private String font;
	private String lorem = readFile("lorem.txt", Charset.defaultCharset());
	private int fontSize, fontStyle;
	private int[] validFontSizes;
	
	public FontSampler() {
		super("Font Sampler");
		setSize(WIDTH, HEIGHT);
		setLayout(new BorderLayout());
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		
		JButton displayButton = new JButton("Display");
		displayButton.addActionListener(this);
		add(displayButton, BorderLayout.NORTH);
		
//		fontDisplayPane = new JPanel();
//		fontDisplayPane.setSize(getContentPane().getWidth(), getContentPane().getHeight());
//		fontDisplayPane.setLayout(new BorderLayout());
		
		displayArea = new JTextArea(5, 5);
		displayArea.setEditable(false);
		displayArea.setLineWrap(true);
		displayArea.setWrapStyleWord(true);
		add(displayArea, BorderLayout.CENTER);
		
//		add(fontDisplayPane, BorderLayout.CENTER);
		
		JMenuBar options = new JMenuBar();
		options.setLayout(new FlowLayout());
		
//		create menu for fonts
		JMenu fontMenu = new JMenu("Font");
		
		String[] fonts = { Font.SERIF, Font.SANS_SERIF, Font.MONOSPACED,
				"Times New Roman", "Estrangelo Edessa"
		};
		
		JMenuItem[] fontItems = new JMenuItem[fonts.length];
		
		for (int i = 0; i < fontItems.length; i++) {
			fontItems[i] = new JMenuItem(fonts[i]);
			fontItems[i].addActionListener(new FontListener());
			fontMenu.add(fontItems[i]);
		}
		
//		create menu for font size
		JMenu fontSizeMenu = new JMenu("Size");
		
		validFontSizes = new int[]{ 9, 10, 12, 14, 16, 24, 32 };
		JMenuItem[] fontSizeItems = new JMenuItem[validFontSizes.length];
		
		for (int i = 0; i < validFontSizes.length; i++) {
			fontSizeItems[i] = new JMenuItem(Integer.toString(validFontSizes[i]));
			fontSizeItems[i].addActionListener(new FontSizeListener());
			fontSizeMenu.add(fontSizeItems[i]);
		}
		
//		create menu for font style
		JMenu fontStyleMenu = new JMenu("Style");
		
		JMenuItem bold = new JMenuItem("BOLD");
		bold.setActionCommand(Integer.toString(Font.BOLD));
		bold.addActionListener(new FontStyleListener());
		fontStyleMenu.add(bold);

		JMenuItem italic = new JMenuItem("ITALIC");
		italic.setActionCommand(Integer.toString(Font.ITALIC));
		italic.addActionListener(new FontStyleListener());
		fontStyleMenu.add(italic);
		
		JMenuItem plain = new JMenuItem("PLAIN");
		plain.setActionCommand(Integer.toString(Font.PLAIN));
		plain.addActionListener(new FontStyleListener());
		fontStyleMenu.add(plain);
		
		options.add(fontMenu);
		options.add(fontSizeMenu);
		options.add(fontStyleMenu);
		
		setJMenuBar(options);
	}
	
	static String readFile(String path, Charset encoding) {
		byte[] encoded = null;
		String result = null;
		try {
			encoded = Files.readAllBytes(Paths.get(path));
			result = encoding.decode(ByteBuffer.wrap(encoded)).toString();
		} catch (IOException i) {
			i.printStackTrace();
			System.exit(0);
		}
		
		return result;
	}

	private class FontListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			font = e.getActionCommand();
		}
		
	}
	
	private class FontStyleListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			fontStyle = Integer.valueOf(e.getActionCommand());
		}
		
	}
	
	private class FontSizeListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			fontSize = Integer.valueOf(e.getActionCommand());
		}
		
	}
	
	public static void main(String[] args) {
		FontSampler fontSampler = new FontSampler();
		fontSampler.setVisible(true);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		try {
			if (null == font) {
				displayArea.setText("You must choose a font");
				return;
			}
			
			if (!isValidFontSize(fontSize)) {
				displayArea.setText("You must choose a valid font size");
				return;
			}
			
			displayArea.setFont(new Font(font, fontStyle, fontSize));
			displayArea.setText(lorem);
		} catch (Exception f) {
			System.out.println(f.getMessage());
			System.out.println(f.getCause());
		}
	}
	
	private boolean isValidFontSize (int fontSizeTested) {
		for (int valid : validFontSizes) {
			if (fontSizeTested == valid) return true;
		}
		return false;
	}
	
}
