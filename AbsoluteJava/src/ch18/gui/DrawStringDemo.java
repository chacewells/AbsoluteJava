package ch18.gui;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;

import javax.swing.JFrame;

public class DrawStringDemo extends JFrame {

	public static void main(String[] args) {
		DrawStringDemo demo = new DrawStringDemo();
		demo.setVisible(true);
	}
	
	public DrawStringDemo() {
		super("Draw String Demo");
		setSize(800, 600);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
	}
	
	public void paint(Graphics g) {
		g.setColor(Color.WHITE);
		g.fillRect(0, 0, getWidth(), getHeight());
		
		g.setColor(Color.BLACK);
		g.setFont(new Font("Estrangelo Edessa", Font.PLAIN, 36));
		
		for (int i = 20; i < (getHeight() - 36); i += 36){
			g.drawString("Is this what you wanted?", 10, i);
			g.drawLine(10, i, getWidth() * 2/3, i);
		}
	}
}