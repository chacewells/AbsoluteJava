package ch18.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class ShapesInPanel extends JFrame {
	public static final int WIDTH = 800;
	public static final int HEIGHT = 600;
	
	
	public static void main(String[] args) {
		ShapesInPanel sip = new ShapesInPanel();
		sip.setVisible(true);
	}
	
	public ShapesInPanel() {
		super("Hello world!");
		setLayout(new BorderLayout());
		setSize(WIDTH, HEIGHT);
		
		PanelObject s = new PanelObject();
		add(s, BorderLayout.CENTER);
		
		JButton click = new JButton("Hi!");
		click.addActionListener(s);
		add(click, BorderLayout.SOUTH);
	}
	
	private class PanelObject extends JPanel implements ActionListener {
		public PanelObject() {
			super();
		}

		public void paintComponent (Graphics g) {
			g.setColor(Color.BLACK);
			g.fillRect(0, 0, getWidth(), getHeight());
		}
		
		public void repaintComponent (Graphics g) {
			g.setColor(Color.BLACK);
			g.fillRect(0, 0, getWidth(), getHeight());
			
			int ovalWidth = 200, ovalHeight = 200;
			g.setColor(Color.WHITE);
			g.fillOval(
					(getWidth()/2)-(ovalWidth/2),
					(getHeight()/2)-(ovalHeight/2),
					ovalWidth,
					ovalHeight);
		}

		@Override
		public void actionPerformed(ActionEvent arg0) {
			repaintComponent(this.getGraphics());
		}
	}
}
