package ch18.gui;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JFrame;

public class Shapes extends JFrame {

	public static final int WIDTH = 600;
	public static final int HEIGHT = 400;
	public static final int ARC_WIDTH = 100;
	public static final int ARC_HEIGHT = 100;
	public static final int ARC_X = (WIDTH/2) - (ARC_WIDTH/2);
	public static final int ARC_Y = (HEIGHT/2) - (ARC_HEIGHT/2);
	public static final int ARC_START = 30;
	public static final int ARC_SWEEP = 300;
	
	public static void main(String[] args) {
		Shapes window = new Shapes();
		window.setVisible(true);
	}

	public Shapes() {
		super("Shapes Demo");
		setSize(WIDTH, HEIGHT);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	
	public void paint(Graphics g) {
		g.setColor(Color.BLACK);
		g.fillRect(0, 0, getWidth(), getHeight());
		
		int xPos = (WIDTH/2) - 10;
		g.setColor(Color.WHITE);
		for (int i=0; i<10; i++) {
			g.fillOval(xPos, (HEIGHT/2)-5, 10, 10);
			xPos += 40;
		}
		
		g.setColor(Color.YELLOW);
		g.fillArc(ARC_X, ARC_Y, ARC_WIDTH, ARC_HEIGHT, ARC_START, ARC_SWEEP);
		
		g.setColor(Color.GREEN);
		g.fillRect(50, 75, 50, 50);
		
		g.fillArc(50, 50, 50, 50, 0, 180);
		
		xPos = 50;
		for (int i = 0; i<5; i++) {
			g.fillOval(xPos, 120, 10, 10);
			xPos += 10;
			if (g.getColor().equals(Color.BLACK))
				g.setColor(Color.GREEN);
			else if (g.getColor().equals(Color.GREEN))
				g.setColor(Color.BLACK);
		}
		
		g.setColor(Color.WHITE);
		g.fillOval(60, 85, 10, 10);
		g.fillOval(80, 85, 10, 10);
		
		g.setColor(Color.BLACK);
		g.fillOval(65, 87, 5, 5);
		g.fillOval(85, 87, 5, 5);
		
		g.setColor(Color.CYAN);
		g.fillRoundRect(WIDTH-200, 50, 100, 100, 100, 50);
	}
}
