package ch18.gui;

import java.awt.Color;
import java.awt.Graphics;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

import javax.swing.JFrame;

public class HumphreyImage extends JFrame {
	public static final int MAX_RGB = 177;
	public static final int MIN_RGB = 12;
	public static final int SCALE = 1;
	private int[][] pixels;

	public static void main(String[] args) {
		HumphreyImage test = new HumphreyImage();
		test.setVisible(true);
	}
	
	HumphreyImage() {
		super("It's the Humphrey Rock!");
		setHumphreyData();
		setSize(pixels.length*SCALE, pixels.length*SCALE);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
	}
	
	public void paint(Graphics g) {
		for (int i = 0; i < pixels.length; i++) {
			for (int j = 0; j < pixels[i].length; j++) {
				g.setColor(getGrayscaleColor(getNewShade(pixels[i][j])));
				g.fillRect(j*SCALE, i*SCALE, SCALE, SCALE);
			}
		}
	}
	
	public Color getGrayscaleColor(int shade) {
		return new Color(shade, shade, shade);
	}
	
	public int getNewShade(int oldShade) {
		return (255 * (oldShade - MIN_RGB)) / (MAX_RGB - MIN_RGB);
	}
	
	public void setHumphreyData() {
		File humphrey = new File("humphrey-img.txt");
		Scanner humphreyIn = null;
		
		try {
			humphreyIn = new Scanner(humphrey);
			int imageSize = humphreyIn.nextInt();
			pixels = new int[imageSize][imageSize];
			for (int i = 0; i < pixels.length; i++) {
				for (int j = 0; j < pixels[i].length; j++) {
					pixels[i][j] = humphreyIn.nextInt();
				}
			}
		} catch(FileNotFoundException fnf) {
			System.out.println("No dice");
		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			humphreyIn.close();
		}
	}
	
}
