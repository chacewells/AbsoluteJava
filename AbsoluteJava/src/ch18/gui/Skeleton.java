package ch18.gui;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;

public class Skeleton extends JFrame implements WindowListener, ActionListener {
	public static final int WIDTH = 600;
	public static final int HEIGHT = 400;
	
	private JLabel eventLabel;
	
	public Skeleton() {
		super("Skeleton Window");
		setSize(WIDTH, HEIGHT);
		setLayout(new BorderLayout());
		setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
		
		addWindowListener(this);
		
		eventLabel = new JLabel();
		add(eventLabel, BorderLayout.CENTER);
		
		JButton closeButton = new JButton("Close");
		closeButton.addActionListener(this);
		add(closeButton, BorderLayout.SOUTH);
	}

	@Override
	public void windowActivated(WindowEvent arg0) {
		eventLabel.setText("Window activated.");
		System.out.println("Window activated.");
	}

	@Override
	public void windowClosed(WindowEvent arg0) {
		System.out.println("Window closed.");
		System.exit(0);
	}

	@Override
	public void windowClosing(WindowEvent arg0) {
		try {
			System.out.println("Window closing.");
			Thread.sleep(500);
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(1);
		}
	}

	@Override
	public void windowDeactivated(WindowEvent arg0) {
		System.out.println("Window deactivated.");
		eventLabel.setText("Window deactivated.");
	}

	@Override
	public void windowDeiconified(WindowEvent arg0) {
		System.out.println("Restored!");
		eventLabel.setText("Restored!");
	}

	@Override
	public void windowIconified(WindowEvent arg0) {
		System.out.println("Window minimized.");
	}

	@Override
	public void windowOpened(WindowEvent arg0) {
		System.out.println("Window opened.");
		eventLabel.setText("Window opened.");
	}

	public static void main(String[] args) {
		Skeleton skel = new Skeleton();
		skel.setVisible(true);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		System.out.println("Exiting program.");
		System.exit(0);
	}

}
