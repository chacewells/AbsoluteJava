package ch18.gui;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.JFrame;
import javax.swing.JPanel;

public class SmileWhenActive extends JFrame {
	private FacePane face = this.new FacePane();
	public static final int WIDTH = 800;
	public static final int HEIGHT= 600;
	public static final int FACE_SIZE = HEIGHT / 2;
	public static final int FACE_X = (WIDTH/2) - (FACE_SIZE/2);
	public static final int FACE_Y = (HEIGHT/2) - (FACE_SIZE/2);
	public static final int EYE_SIZE = FACE_SIZE / 8;
	public static final int EYE_LEFT_X = FACE_X + (FACE_SIZE / 4);
	public static final int EYE_LEFT_Y = FACE_Y + (FACE_SIZE / 4);
	public static final int EYE_RIGHT_X = EYE_LEFT_X + 2 * (FACE_SIZE / 4) - EYE_SIZE;
	public static final int EYE_RIGHT_Y = EYE_LEFT_Y; 
	public static final int MOUTH_SIZE = FACE_SIZE / 2;
	public static final int SMILE_X = (WIDTH/2) - (MOUTH_SIZE/2);
	public static final int SMILE_Y = (HEIGHT/2) - (MOUTH_SIZE/2);
	public static final int FROWN_X = SMILE_X;
	public static final int FROWN_Y = SMILE_Y + (MOUTH_SIZE/2);
	
	private boolean smile = true;
	
	public static void main(String[] args) {
		SmileWhenActive test = new SmileWhenActive("Happy or Sad");
		test.setVisible(true);
	}
	
	public SmileWhenActive(String title) {
		super(title);
		setSize(WIDTH, HEIGHT);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		addWindowListener(this.new FrownWhenNot());
		add(face);
	}
	
	private class FacePane extends JPanel {
		public FacePane() {
			super();
		}
		
		public void paintComponent(Graphics g) {
			g.setColor(Color.BLACK);
			g.fillRect(0, 0, getWidth(), getHeight());
			
			g.setColor(Color.YELLOW);
			g.fillOval(FACE_X, FACE_Y, FACE_SIZE, FACE_SIZE);
			
			g.setColor(Color.BLACK);
			g.fillOval(EYE_LEFT_X, EYE_LEFT_Y, EYE_SIZE, EYE_SIZE);
			
			g.fillOval(EYE_RIGHT_X, EYE_RIGHT_Y, EYE_SIZE, EYE_SIZE);
			
			if (smile)
				g.drawArc(SMILE_X, SMILE_Y, MOUTH_SIZE, MOUTH_SIZE, 180, 180);
			else
				g.drawArc(FROWN_X, FROWN_Y, MOUTH_SIZE, MOUTH_SIZE, 0, 180);
		}
		
	}
	
	private class FrownWhenNot implements WindowListener {
		@Override
		public void windowActivated(WindowEvent e) {
			smile = true;
			face.repaint();
		}

		@Override
		public void windowClosed(WindowEvent e) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void windowClosing(WindowEvent e) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void windowDeactivated(WindowEvent e) {
			smile = false;
			face.repaint();
		}

		@Override
		public void windowDeiconified(WindowEvent e) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void windowIconified(WindowEvent e) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void windowOpened(WindowEvent e) {
			// TODO Auto-generated method stub
			
		}
	}

}
