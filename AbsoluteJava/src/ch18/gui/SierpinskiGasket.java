package ch18.gui;

import java.awt.Color;
import java.awt.Graphics;
import java.util.Arrays;
import java.util.Random;

import javax.swing.JFrame;

public class SierpinskiGasket extends JFrame {
	public static final int WIDTH = 600;
	public static final int HEIGHT= 600;
	public static final int Xx = WIDTH/2;
	public static final int Xy = HEIGHT/4;
	public static final int Yx = Xx/2;
	public static final int Yy = (Xy + HEIGHT)/2;
	public static final int Zx = (Xx + WIDTH)/2;
	public static final int Zy = (Xy + HEIGHT)/2;

	public static void main(String[] args) {
		SierpinskiGasket sierGask = new SierpinskiGasket();
		sierGask.setVisible(true);
	}
	
	public SierpinskiGasket() {
		super();
		setSize(WIDTH, HEIGHT);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
	}
	
	public void paint(Graphics g) {
		g.setColor(Color.BLACK);
		g.fillRect(0, 0, this.getWidth(), this.getHeight());
		
		g.setColor(Color.GREEN);
		g.drawLine(Xx, Xy, Xx, Xy);
		g.drawLine(Yx, Yy, Yx, Yy);
		g.drawLine(Zx, Zy, Zx, Zy);
		
		int[] current = {Xx, Xy};
		int[] tempArray = new int[2];
		int[][] points = new int[3][2];
			points[0][0] = Xx;
			points[0][1] = Xy;
			points[1][0] = Yx;
			points[1][1] = Yy;
			points[2][0] = Zx;
			points[2][1] = Zy;
		
		Random rand = new Random();
		int p;
		
//		System.out.printf("X: %d, %d\n", current[0], current[1]);
//		System.out.printf("Y: %d, %d\n", Yx, Yy);
//		System.out.printf("Z: %d, %d\n", Zx, Zy);
		
		
		for (int i = 0; i < 100000; i++) {
			p = (int) Math.floor(Math.random() * 3);
			if (i % 50000 == 0) {
				g.setColor(new Color(
						rand.nextInt(256),
						rand.nextInt(256),
						rand.nextInt(256)
						));
			}
			
			tempArray[0] = (current[0] + points[p][0]) / 2;
			tempArray[1] = (current[1] + points[p][1]) / 2;
			
			current = Arrays.copyOf(tempArray, tempArray.length);
			
			g.drawLine(current[0], current[1], current[0], current[1]);
//			System.out.printf("after change: %d, %d\n", current[0], current[1]);
		}
	}

}
