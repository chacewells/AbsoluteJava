package ch18.gui;

import java.awt.BorderLayout;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

public class ScrollBarDemo extends JFrame {

	public static final int WIDTH = 600;
	public static final int HEIGHT = 400;
	
	public static void main(String[] args) {
		ScrollBarDemo demo = new ScrollBarDemo("Scroll Pane Demo");
		demo.setVisible(true);
	}
	
	public ScrollBarDemo(String title) {
		super(title);
		setLayout(new BorderLayout());
		setSize(WIDTH, HEIGHT);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		JTextArea content = new JTextArea(50, 100);
		
		JScrollPane scroller = new JScrollPane(content);
		scroller.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
		scroller.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		
		add(scroller);
	}
	
}
