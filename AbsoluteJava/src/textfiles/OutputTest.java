package textfiles;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

public class OutputTest {
	public static void main(String[] bloggie) {
		try {
			ObjectOutputStream output = 
					new ObjectOutputStream(
					new FileOutputStream("numbers.dat"));
			
			int i;
			for (i = 0; i < 5; i++)
				output.writeInt(i);
			
			System.out.println("numbers written to the file numbers.dat");
			output.close();
		} catch (IOException e) {
			System.out.println("Problem with file output.");
		}
	}
}
