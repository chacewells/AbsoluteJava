package textfiles;

import java.io.EOFException;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;

public class InputTest {
	public static void main(String[] bloggie) {
		try {
			ObjectInputStream inputStream = 
					new ObjectInputStream(
					new FileInputStream("numbers.dat"));
			
			System.out.println("Reading numbers from numbers.dat:");
			
			try {
				while (true) {
					System.out.println(inputStream.readInt());
				}
			} catch (EOFException e) {
				// do nothing
			}
			
			inputStream.close();
		} catch(FileNotFoundException f) {
			System.out.println("File could not be opened.");
		} catch (IOException e) {
			System.out.println("Problem with file output.");
		}
	}
}
