package textfiles;

import java.io.File;
import java.util.Scanner;

public class DeleTest {
	public static void main(String[] files) {
		File fileThouDeletest = new File(files[0]);
		String userInput;
		Scanner scribe = new Scanner(System.in);
		
		if(!fileThouDeletest.exists()) {
			System.out.println("Know, lord user, there existeth no file wherewith"
					+ " thou hast named.  I am therefore obliged to exit.");
			System.exit(0);
		}
		
		System.out.println("Are thou sure, O illustrious user, that the"
				+ " deletion of file " + fileThouDeletest.getName()
				+ " be the desire of thine heart?");
		userInput = scribe.nextLine();
		
		try {
			if(userInput.equalsIgnoreCase("no"))
				System.out.println("Be this deletion ignored in accordance with thy wisdom.");
			else {
				fileThouDeletest.delete();
				System.out.println("Know, oh lord user, that henceforth the file "
						+ fileThouDeletest.getName() + " hath been deleted and shall"
						+ " no more plague thy filesystem.");
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		scribe.close();
	}
}
