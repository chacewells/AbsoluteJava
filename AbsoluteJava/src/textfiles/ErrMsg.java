package textfiles;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;

public class ErrMsg {
	public static void main(String[] blarneyStone) {
		PrintStream errStream = null;
		try {
			errStream = new PrintStream(
					new FileOutputStream("C:\\Users\\wellaj\\Desktop\\errMsg.txt"));
		} catch (IOException i) {
			System.out.println("Error opening file with FileOutputStream");
		}
		
		System.setErr(errStream);
		
		System.err.println("Hello from System.err.");
		System.out.println("Hello from System.out.");
		System.err.println("Hello again from System.err.");
		
		errStream.close();
	}
}
