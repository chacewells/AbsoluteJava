package textfiles;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

public class HasNext {
	public static void main(String[] hasNext) {
		System.out.println("Building queries from 'DB.txt' to 'Queries.txt'");
		
		Scanner fileScan = null;
		
		try {
			fileScan = new Scanner(new FileInputStream("C:\\Users\\wellaj\\Desktop\\DB.txt"));
		} catch(IOException i) {
			System.out.println("Cannot open file 'DB.txt':");
			System.out.println("Exiting");
			System.exit(0);
		}
		
		PrintWriter writeQueries = null;
		try {
			writeQueries = new PrintWriter(
					new FileOutputStream("C:\\Users\\wellaj\\Desktop\\Queries.txt"));
		} catch (IOException i) {
			System.out.println("Cannot open writeto file.");
			System.exit(0);
		}
		
		int a, b, c;
		while (fileScan.hasNext()) {
			a = fileScan.hasNextInt() ? fileScan.nextInt() : null;
			b = fileScan.hasNextInt() ? fileScan.nextInt() : null;
			c = fileScan.hasNextInt() ? fileScan.nextInt() : null;
			
			String f = String.format("INSERT INTO table"
					+ " VALUES (%d, %d, %d);", a, b, c);
			
			writeQueries.println(f);
		}
		
		System.out.println("All done!");
		
		fileScan.close();
		writeQueries.close();
	}
}
