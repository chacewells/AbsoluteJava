package textfiles;

import java.io.File;

public class FileTest {
	public static void main(String[] args) {
		File testFile = new File("Sally.txt");
		
		if (!testFile.exists())
			System.out.println(testFile.getName()+" does not exist.");
		else
			System.out.println("Good work. You found "+testFile.getName());
	}
}
