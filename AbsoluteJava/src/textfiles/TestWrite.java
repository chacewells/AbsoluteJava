package textfiles;

import java.io.FileOutputStream;
import java.io.PrintWriter;

public class TestWrite {
	public static void main(String[] popCorn) {
		PrintWriter toFile = null;
		try {
			toFile = new PrintWriter
					(new FileOutputStream("C:\\Users\\wellaj\\Desktop\\DB.txt"));
		} catch(Exception e) {
			System.out.println("Cannot open file.");
			System.exit(0);
		}
		
		System.out.println("Writing to file.");
		
		int i, j, k;
		String query;
		
		for(i = 0; i < 20; i++) {
			j = (i * 10) + 2;
			k = (i * 4) +3;
			query = String.format("%d\t%d\t%d%n", i, j, k);
			
			toFile.append(query);
		}
		
		toFile.close();
		System.out.println("All done!");
	}
}
