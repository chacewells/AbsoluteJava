package textfiles;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Scanner;

public class TestRead {
	public static void main(String[] fargs) {
		System.out.println("Here's some database queries:");
		
		Scanner fileStream = null;
		
		try {
			fileStream = new Scanner(
					new FileInputStream("C:\\Users\\wellaj\\Desktop\\DB.txt"));
		} catch (IOException i) {
			System.out.println(i.getMessage());
			System.exit(0);
		}
		
		String q1 = fileStream.nextLine();
		String q2 = fileStream.nextLine();
		String q3 = fileStream.nextLine();
		
		System.out.println("Here are the queries you wanted:");
		System.out.printf("%s%n%s%n%s%n", q1, q2, q3);
		
		fileStream.close();
	}
}
