package textfiles;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

public class NumberLines {
	public static void main(String[] falrman) {
		Scanner queries = null;
		PrintWriter numberedQueries = null;
		
		try {
			queries = new Scanner(
					new FileInputStream("C:\\Users\\wellaj\\Desktop\\Queries.txt"));
			numberedQueries = new PrintWriter(
					new FileOutputStream("C:\\Users\\wellaj\\Desktop\\NumberedQueries.txt"));
		} catch (IOException i) {
			System.out.println(i.getMessage());
			System.exit(0);
		}
		
		String line;
		int count = 0;

		System.out.println("Now writing numbered queries to NumberedQueries.txt");
		while (queries.hasNextLine()) {
			line = queries.nextLine();
			numberedQueries.println(++count + "\t" + line);
		}
		System.out.println("All done!");
		
		queries.close();
		numberedQueries.close();
	}
}
