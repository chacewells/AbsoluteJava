package counter;

public class CounterModel {
	int counter;
	
	public CounterModel(int initialValue) {
		this.counter = initialValue;
	}
	
	public void Increment() {
		try {
			CounterView.Display(this.counter);
			counter++;
			Thread.sleep(1000);
		} catch (InterruptedException i) {
			System.out.println(i.getMessage());
		}
	}
}
