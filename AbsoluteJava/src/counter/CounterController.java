package counter;

import java.util.Scanner;

public class CounterController {
	private int startValue, endValue;
	
	public static void main(String[] args) {
		Scanner keys = new Scanner(System.in);
		
		System.out.println("Where to start?");
			int start = keys.nextInt();
		System.out.println("Where to end?");
			int end = keys.nextInt();
		CounterController cont = new CounterController(start, end);

		cont.Start();
	}
	
	public CounterController(int startValue, int endValue) {
		this.startValue = startValue;
		this.endValue = endValue;
	}
	
	public void Start() {
		CounterModel ctr = new CounterModel(this.startValue);
		while(ctr.counter <= this.endValue) {
			ctr.Increment();
		}
		System.out.println("All done!");
	}
}
