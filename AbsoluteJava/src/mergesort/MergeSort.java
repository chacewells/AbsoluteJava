package mergesort;

import java.util.Arrays;

public class MergeSort {
	
	public static void main(String[] args) {
		int[] nums = new int[1000];
		int tmp;
		
		for (int i = 0; i < nums.length; i++) {
			tmp = (int) Math.floor(Math.random() * 400000);
			nums[i] = tmp;
		}
		
		for (int i = 0; i < nums.length; i++) {
			if (i % 10 == 0)
				System.out.println();
			System.out.printf("% 6d ", nums[i]);
		}
		
		System.out.println();

		long startTime = System.nanoTime();
		nums = sort(nums);
		long endTime = System.nanoTime();
		
		for (int i = 0; i < nums.length; i++) {
			if (i % 10 == 0)
				System.out.println();
			System.out.printf("% 5d", nums[i]);
		}
		long totalTime = endTime - startTime;
		System.out.printf("\n%.6f secs", (double) totalTime * .000000001);
	}
	
	public static int[] sort(int[] myArray) {
		if (myArray.length <= 1)
			return myArray;
		
		int mid = split(myArray);
		int[] left = sort(Arrays.copyOfRange(myArray, 0, mid));
		int[] right = sort(Arrays.copyOfRange(myArray, mid, myArray.length));
		return merge(left, right);
	}
	
	private static int split(int[] myArray) {
		return (int) Math.floor((double) myArray.length / 2);
	}
	
	private static int[] merge(int[] left, int[] right) {
		int iL, iR, iRes;
		iL = iR = iRes = 0;
		int[] result = new int[left.length + right.length];
		
		while ((iL < left.length) && (iR < right.length)) {
			if (left[iL] <= right[iR]) {
				result[iRes] = left[iL];
				iL++;
				iRes++;
			} else {
				result[iRes] = right[iR];
				iR++;
				iRes++;
			}
		}
		
		while (iL < left.length) {
			result[iRes] = left[iL];
			iL++;
			iRes++;
		}
		
		while (iR < right.length) {
			result[iRes] = right[iR];
			iR++;
			iRes++;
		}
		
		return result;
	}
}
