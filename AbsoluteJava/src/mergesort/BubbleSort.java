package mergesort;

public class BubbleSort {
	
	public static void main(String[] args) {
		int[] nums = new int[1000];
		int tmp;
		
		for (int i = 0; i < nums.length; i++) {
			tmp = (int) Math.floor(Math.random() * 400000);
			nums[i] = tmp;
		}
		
		for (int i = 0; i < nums.length; i++) {
			if (i % 10 == 0)
				System.out.println();
			System.out.printf("% 6d ", nums[i]);
		}
		
		System.out.println();

		long startTime = System.nanoTime();
		nums = sort(nums);
		long endTime = System.nanoTime();
		
		for (int i = 0; i < nums.length; i++) {
			if (i % 10 == 0)
				System.out.println();
			System.out.printf("% 5d", nums[i]);
		}
		long totalTime = endTime - startTime;
		System.out.printf("%.6f secs", (double) totalTime * .000000001);
	}
	
	public static int[] sort(int[] myArray) {
		int tmp;
		int count;
		
		for (int i = 0; i < myArray.length - 1; i++) {
			count = 0;
			for (int j = i; j < myArray.length; j++) {
				if (myArray[j] < myArray[i]) {
					tmp = myArray[j];
					myArray[j] = myArray[i];
					myArray[i] = tmp;
					count++;
				}
			}
			if (count < 1)
				break;
		}
		
		return myArray;
	}
}
