package testbin;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class TestDummyIO {
	public static void main(String[] args) {
		DummyClass[] dummy = new DummyClass[3];
		
		dummy[0] = new DummyClass("first dummy");
		dummy[1] = new DummyClass("second dummy");
		dummy[2] = new DummyClass("third dummy");
		
		try {
			ObjectOutputStream dummyOut = new ObjectOutputStream(
					new FileOutputStream("dummy.dat"));
			dummyOut.writeObject(dummy);
			dummyOut.close();
		} catch (IOException i) {
			System.out.println("Error writing");
			System.exit(0);
		}
		
		System.out.println("dummy array written");
		System.out.println("now i'll reopen and write the toString() for each dummy");
		
		DummyClass[] getDummy = null;
		
		try {
			ObjectInputStream dummyIn =
					new ObjectInputStream(new FileInputStream("dummy.dat"));
			getDummy = (DummyClass[])dummyIn.readObject();
			dummyIn.close();
		} catch (FileNotFoundException f) {
			System.out.println("File not found");
			System.exit(0);
		} catch (ClassNotFoundException c) {
			System.out.println("Couldn't find the array we just put in there!");
			System.exit(0);
		} catch (IOException i) {
			System.out.println("Something wrong with file input");
			System.exit(0);
		}
		System.out.println("The follow array elements were found:");
		
		for (int i = 0; i < getDummy.length; i++) {
			System.out.println(getDummy[i].toString());
		}
	}
}
