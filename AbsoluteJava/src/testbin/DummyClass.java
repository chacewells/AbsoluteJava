package testbin;
import java.io.Serializable;

public class DummyClass implements Serializable {
	private String str;
	
	public DummyClass () {
		this.str = "dummy";
	}
	
	public DummyClass (String str) {
		this.str = str;
	}
	
	public String getStr() {
		return str;
	}
	
	public void setStr(String str) {
		this.str = str;
	}
	
	public String toString() {
		return this.getStr();
	}
}
