package ch15.linkedlists;

public class StringWrapper implements PubliclyCloneable{
	private String str;
	
	public StringWrapper(String theString){
		str = theString;
	}
	
	public String toString(){
		return str;
	}
	
	public void setString(String newString){
		str = newString;
	}
	
	public Object clone() {
		try{
			StringWrapper strWrap = (StringWrapper) super.clone();
			strWrap.setString(toString());
			return strWrap;
		}catch(CloneNotSupportedException e){
			return null;
		}
	}
}
