package ch15.linkedlists;

import java.util.ArrayList;
import java.util.ListIterator;

public class HugeNumber {
	public SigDig mostSig;

	public static void main(String[] args) {
		String something = "";
		char[] chars = {'a','a','r','o','n'};
		for (char c : chars)
			something = something.concat(String.valueOf(c));
		System.out.println(something);
	}
	
	private static class SigDig {
		public Character value;
		public SigDig next;
		public SigDig previous;
		
		public SigDig(Character value) {
			this.value = value;
			next = null;
			previous = null;
		}
		
		public boolean hasNext(){
			return next != null;
		}
	}
	
	public HugeNumber() {
		mostSig = null;
	}
	
	public void add(SigDig newSigDig) {
		if (isEmpty()){
			mostSig = newSigDig;
			mostSig.next = null;
			mostSig.previous = null;
		}else{
			newSigDig.next = mostSig;
			mostSig.previous = newSigDig;
			mostSig = newSigDig;
		}
	}
	
	public void remove(SigDig oldSigDig) {
		if(isEmpty()){
			System.out.println("nothing to remove");
		}else if((oldSigDig == mostSig) && (oldSigDig.next == null)){
			mostSig = null;
		}else if((oldSigDig.previous != null) && (oldSigDig.next != null)){
			oldSigDig.previous.next = oldSigDig.next;
			oldSigDig.next.previous = oldSigDig.previous;
		}else if(oldSigDig.previous == mostSig && (oldSigDig.next != null)){
			mostSig = mostSig.next;
			mostSig.previous = null;
		}else{ // ((oldSigDig.previous != null) && (oldSigDig.next == null))
			oldSigDig.previous.next = null;
		}
	}
	
	public boolean isEmpty(){
		return mostSig == null;
	}
	
	public int size(){
		int result = 0;
		SigDig theSigDig = mostSig;
		if (theSigDig == null)
			return 0;
		else if (theSigDig.next == null){
			return 1;
		}else{
			do {
				result++;
				theSigDig = theSigDig.next;
			}while(theSigDig.hasNext());
		}
		return result;
	}
	
	public void setValue(String hugeNumVal) {
		for (int i = hugeNumVal.length(); i >= 0; i--) {
			add(new SigDig(hugeNumVal.charAt(i)));
		}
	}
	
	public String toString() {
		SigDig theSigDig = mostSig;
		String result = "";
		
		while (theSigDig.hasNext()) {
			result = result.concat(String.valueOf(theSigDig.value));
			theSigDig = theSigDig.next;
		}
		
		return result;
	}

}
