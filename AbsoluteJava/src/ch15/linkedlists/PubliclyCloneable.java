package ch15.linkedlists;

public interface PubliclyCloneable extends Cloneable {
	public Object clone();
}
