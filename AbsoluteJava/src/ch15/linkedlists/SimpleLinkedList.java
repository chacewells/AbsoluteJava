package ch15.linkedlists;

public class SimpleLinkedList<T extends PubliclyCloneable>
	implements PubliclyCloneable{
	
	private class Node<T>{
		private T data;
		private Node<T> next;
		
		public Node(){
			
			data = null;
			next = null;
			
		} // Node definition
		
		public Node(T data, Node<T> next){
			
			this.data = data;
			this.next = next;
			
		}
	} // Node<T> inner class
	
	private Node<T> head;
	
	public SimpleLinkedList(){ head = null; }
	
	public SimpleLinkedList(SimpleLinkedList<T> otherList){
		if(otherList == null)
			throw new NullPointerException();
		if(otherList.head == null)
			head = null;
		else
			head = copyOf(otherList.head);
	}
	
	public void add(T itemData){ head = new Node<T>(itemData, head); }
	
	public boolean removeHead(){
		if(head != null){
			head = head.next;
			return true;
		}else{
			return false;
		}
	} // removeHead()
	
	public SimpleLinkedList<T> clone(){
		try{
			SimpleLinkedList<T> copy = (SimpleLinkedList<T>) super.clone();
			if(head == null)
				copy.head = null;
			else
				copy.head = copyOf(head);
			return copy;
		}catch(CloneNotSupportedException e){
			return null; // do what Oracle wants
		}
	}
	
	private Node<T> copyOf(Node<T> otherHead){
		Node<T> position = otherHead; // moves down other's list
		Node<T> newHead; // will point to head of the copy list
		Node<T> end = null; // positioned at end of new growing list
		
		// make first node
		newHead = new Node<T>((T) (position.data).clone(), null);
		end = newHead;
		position = position.next;
		
		while(position != null){
			end.next = new Node<T>((T) (position.data).clone(), null);
			end = end.next;
			position = position.next;
		}
		
		return newHead;
	}
	
	public String toString(){
		Node<T> position = head;
		String theString = "";
		while (position != null){
			theString = theString + position.data + "\n";
			position = position.next;
		}
		return theString;
	}
	
	public int size(){
		int count = 0;
		Node<T> position = head;
		
		while(position != null){
			count++;
			position = position.next;
		}
		
		return count;
	} // size()
	
	public void display(){
		Node<T> position = head;
		while (position != null){
			System.out.println(position.data);
			position = position.next;
		}
	}
	
	public boolean isEmpty(){ return head == null; }
	
	public void clear(){ head = null; }
	
}
