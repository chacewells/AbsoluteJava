package ch15.linkedlists;

public class UseSimple {

	public static void main(String[] args) {
		SimpleLinkedList<StringWrapper> strList = new SimpleLinkedList<>();
		strList.add(new StringWrapper("Hello"));
		strList.add(new StringWrapper("What'r name?"));
		strList.add(new StringWrapper("My name Jim"));
		
		strList.display();
		
		System.out.println();
		
		SimpleLinkedList<StringWrapper> strList2 = strList.clone();
		strList2.removeHead();
		
		strList.display();
		System.out.println();
		strList2.display();
	}

}
