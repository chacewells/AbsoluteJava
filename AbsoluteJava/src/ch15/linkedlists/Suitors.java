package ch15.linkedlists;

public class Suitors {
	public Suitor head;

	public static void main(String[] args) {
		Suitors mySuitors = new Suitors();
		for (int i = 0; i < 22; i++)
			mySuitors.add();
//		mySuitors.display();
		Suitor theSuitor = mySuitors.head;
		
		boolean done = false;
		int count = 0;
		while (!done) {
			if (theSuitor.next.number == theSuitor.number)
				done = true;
			else {
//				System.out.printf("suitor # %d%n" + 
//						"next suitor # %d%n%n", theSuitor.number, theSuitor.next.number);
				theSuitor = theSuitor.next.next.next;
				mySuitors.remove(theSuitor);
				count++;
				if (count > 0 && count % 5 == 0) {
					mySuitors.display();
					System.out.println();
				}
			}
		}
		
		System.out.println("And the winner is...");
		mySuitors.display();
	}
	
	private static class Suitor {
		public Suitor previous;
		public Suitor next;
		public int number;
	}
	
	public Suitors() {
		head = null;
	}
	
	public void add() {
		Suitor newSuitor = new Suitor();
		if (isEmpty()) {
			head = newSuitor;
			newSuitor.number = 1;
			newSuitor.next = head;
			newSuitor.previous = head;
		} else if (getHighestSuitor() == head) {
			head.next = newSuitor;
			newSuitor.next = head;
			newSuitor.previous = head;
			newSuitor.number = head.number + 1;
		} else {
			Suitor lastSuitor = getHighestSuitor();
			lastSuitor.next = newSuitor;
			newSuitor.next = head;
			newSuitor.previous = lastSuitor;
			newSuitor.number = lastSuitor.number + 1;
			head.previous = newSuitor;
		}
	}
	
	public void remove(Suitor theSuitor) {
		if (isEmpty()) {
			System.out.println("No suitors to remove!");
		} else if (getHighestSuitor().number == 1) {
			head = null;
		} else if (theSuitor == head) {
			head.previous.next = theSuitor.next;
			head.next.previous = theSuitor.previous;
			head = theSuitor.next;
		} else {
			theSuitor.previous.next = theSuitor.next;
			theSuitor.next.previous = theSuitor.previous;
		}
	}
	
	public boolean isEmpty() {
		return head == null;
	}

	public Suitor getHighestSuitor() {
		Suitor theSuitor = head;
		while (true) {
			if (theSuitor.next == head)
				return theSuitor;
			theSuitor = theSuitor.next;
		}
	}
	
	public void display() {
		if (head == null) {
			System.out.println("Empty list");
			return;
		}
		Suitor theSuitor = head;
		do {
			System.out.printf("suitor %d%n", theSuitor.number);
			theSuitor = theSuitor.next;
		} while (theSuitor != head);
	}

}
